import React from 'react';

const Task = props => {

    return (
        <div
            className={props.doneStatus ? 'task task--done' : 'task'}
        >
            <input disabled={props.doneStatus ? true : false} data-id={props.id} onChange={props.change} className='task-name' placeholder='Enter New Task' value={props.name}/>
            <span onClick={props.doneClick} className='task-btn task-btn--done'>{props.doneStatus ? 'Reopen' : 'Done'}</span>
            <span
                onClick={props.delete}
                className='task-btn task-btn--del'
            >
                Delete
            </span>
        </div>
    );
}

export default Task;
