import React, { useState, useEffect } from 'react';
import './App.css';
import Task from '../components/Task/Task';

const App = () => {

  let localStor = [];

  if (JSON.parse(localStorage.getItem('tasks'))) {
    localStor = JSON.parse(localStorage.getItem('tasks'));
  }

  const [ taskState, createTaskState] = useState({
    tasks: localStor
  })

  useEffect(() => {
    localStorage.setItem('tasks', JSON.stringify(taskState.tasks));
  }, [taskState])

  const handleCreateTask = () => {
    createTaskState({
      tasks: [
        ...taskState.tasks,
        {name: '', done: false}
      ]
    });
  }

  const handleDoneTask = index => {
    const changes = taskState.tasks;
    changes.splice(index, 1, {name: taskState.tasks[index].name, done: !taskState.tasks[index].done});
    createTaskState({
      tasks: changes
    });
  }

  const handleDeleteTask = index => {
    const result = taskState.tasks;
    result.splice(index, 1);
    createTaskState({
      tasks: result
    })
  }

  const handleChangeTask = event => {
    const id = event.target.dataset.id;
    const changes = taskState.tasks;
    changes.splice(id, 1, {name: event.target.value, done: false});
    createTaskState({
      tasks: changes
    })
  }

  return (
    <div className="App">
      <h1>AwesomeToDoList</h1>
      <div className='tasks'>
        {
          taskState.tasks.map((task, index) => {
            return <Task key={index} id={index} change={handleChangeTask} doneClick={() => handleDoneTask(index)} delete={() => handleDeleteTask(index)} name={task.name} doneStatus={task.done} value={task.name} />
          })
        }
        <div className='task task--creator' onClick={handleCreateTask}>+</div>
      </div>
    </div>
  );
}

export default App;
